var URLUtils = require("dw/web/URLUtils");

server.get("Show", function (req, res, next) {
    var template = "testing";
    res.render(template);
    next();
});

server.get("Product", function (req, res, next) {
    var template = "product";
    var myproduct = { pid: "701644329402M" };
    var ProductFactory = require("*/cartridge/scripts/factories/product");
    var product = ProductFactory.get(myproduct);

    res.render(template, {
        product: product,
    });
    next();
});